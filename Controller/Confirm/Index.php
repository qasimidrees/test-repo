<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Controller\Confirm;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\InvoiceOrderInterface;
use Scalapay\Scalapay\Gateway\Settings;
use Scalapay\Scalapay\Model\DelayedCapture;
use Scalapay\Scalapay\Model\PlaceOrder;
use Scalapay\Scalapay\Model\ResourceModel\GetQuoteByReservedOrderId;
use Scalapay\Scalapay\Model\ResourceModel\SaveScalapayQuoteToken;

class Index implements HttpGetActionInterface
{
    /**
     * @var RedirectFactory
     */
    private $redirectFactory;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var InvoiceOrderInterface
     */
    private $invoiceOrder;
    /**
     * @var Session
     */
    private $checkoutSession;
    /**
     * @var GetQuoteByReservedOrderId
     */
    private $getQuoteByReservedOrderId;
    /**
     * @var SaveScalapayQuoteToken
     */
    private $saveScalapayQuoteToken;
    /**
     * @var Settings
     */
    private $settings;
    /**
     * @var PlaceOrder
     */
    private $placeOrder;
    /**
     * @var DelayedCapture
     */
    private $delayedCapture;

    /**
     * Index constructor.
     * @param RedirectFactory $redirectFactory
     * @param RequestInterface $request
     * @param ManagerInterface $messageManager
     * @param InvoiceOrderInterface $invoiceOrder
     * @param Session $checkoutSession
     * @param GetQuoteByReservedOrderId $getQuoteByReservedOrderId
     * @param SaveScalapayQuoteToken $saveScalapayQuoteToken
     * @param Settings $settings
     * @param PlaceOrder $placeOrder
     * @param DelayedCapture $delayedCapture
     */
    public function __construct(
        RedirectFactory $redirectFactory,
        RequestInterface $request,
        ManagerInterface $messageManager,
        InvoiceOrderInterface $invoiceOrder,
        Session $checkoutSession,
        GetQuoteByReservedOrderId $getQuoteByReservedOrderId,
        SaveScalapayQuoteToken $saveScalapayQuoteToken,
        Settings $settings,
        PlaceOrder $placeOrder,
        DelayedCapture $delayedCapture
    ) {
        $this->redirectFactory = $redirectFactory;
        $this->request = $request;
        $this->messageManager = $messageManager;
        $this->invoiceOrder = $invoiceOrder;
        $this->checkoutSession = $checkoutSession;
        $this->getQuoteByReservedOrderId = $getQuoteByReservedOrderId;
        $this->saveScalapayQuoteToken = $saveScalapayQuoteToken;
        $this->settings = $settings;
        $this->placeOrder = $placeOrder;
        $this->delayedCapture = $delayedCapture;
    }

    /**
     * @return Redirect
     * @noinspection PhpUndefinedMethodInspection
     */
    public function execute(): Redirect
    {
        $status = true;
        $orderToken = $this->request->getParam('orderToken');
        $quoteId = $this->getQuoteByReservedOrderId->execute($this->checkoutSession->getReservedOrderId());
        $this->saveScalapayQuoteToken->execute($quoteId, $orderToken);
        $this->checkoutSession->unsPlaceOrder();
        try {
            $orderId = $this->placeOrder->execute($this->checkoutSession, $quoteId, $orderToken);
            if ($this->settings->getDelayedCapture()) {
                $this->delayedCapture->execute($orderId);
            } else {
                $this->invoiceOrder->execute($orderId, true);
            }
        } catch (LocalizedException | AuthorizationException $e) {
            $status = false;
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        if ($status) {
            $path = 'checkout/onepage/success';
        } else {
            $path = 'checkout/onepage/failure';
        }

        $this->checkoutSession->unsReservedOrderId();
        /** @var \Magento\Framework\Controller\Result\Redirect $redirect */
        $redirect = $this->redirectFactory->create();
        return $redirect->setPath($path, ['_secure' => true]);
    }
}
