<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\Message\Manager;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Scalapay\Scalapay\Model\ResourceModel\GetScalapayOrderToken;
use Scalapay\Scalapay\Factory\Api as ScalapayApi;

class Capture implements ClientInterface
{
    const CAPTURE_STATUS_OK = 'APPROVED';

    /**
     * @var GetAuthorization
     */
    private $getAuthorization;
    /**
     * @var ScalapayApi
     */
    private $scalapayApi;
    /**
     * @var Manager
     */
    private $messageManager;
    /**
     * @var GetScalapayOrderToken
     */
    private $getScalapayOrderToken;

    /**
     * Capture constructor.
     * @param GetAuthorization $getAuthorization
     * @param ScalapayApi $scalapayApi
     * @param Manager $messageManager
     * @param GetScalapayOrderToken $getScalapayOrderToken
     */
    public function __construct(
        GetAuthorization $getAuthorization,
        ScalapayApi $scalapayApi,
        Manager $messageManager,
        GetScalapayOrderToken $getScalapayOrderToken
    ) {
        $this->getAuthorization = $getAuthorization;
        $this->scalapayApi = $scalapayApi;
        $this->messageManager = $messageManager;
        $this->getScalapayOrderToken = $getScalapayOrderToken;
    }

    /**
     * @inheritDoc
     */
    public function placeRequest(TransferInterface $transferObject): array
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $transferObject->getBody()['payment']->getOrder();
        try {
            $orderToken = $this->getScalapayOrderToken->execute((int) $order->getEntityId());
            /** @noinspection PhpUndefinedMethodInspection */
            $order->setScalapayOrderToken($orderToken);
            $transferObject->getBody()['payment']->setTransactionId($orderToken);
            $status = $this->scalapayApi->capture(
                $this->getAuthorization->execute(),
                $orderToken
            );
            if ($status !== self::CAPTURE_STATUS_OK) {
                throw new AuthorizationException(__('Payment capture was not authorized.'));
            }

        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return [];
    }
}
