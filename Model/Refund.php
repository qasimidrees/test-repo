<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Framework\Message\ManagerInterface;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Scalapay\Scalapay\Factory\Api as ScalapayApi;

class Refund implements ClientInterface
{
    /**
     * @var ScalapayApi
     */
    private $scalapayApi;
    /**
     * @var GetAuthorization
     */
    private $getAuthorization;
    /**
     * @var GetOrderRefund
     */
    private $getOrderRefund;
    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * Refund constructor.
     * @param ScalapayApi $scalapayApi
     * @param GetAuthorization $getAuthorization
     * @param GetOrderRefund $getOrderRefund
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        ScalapayApi $scalapayApi,
        GetAuthorization $getAuthorization,
        GetOrderRefund $getOrderRefund,
        ManagerInterface $messageManager
    ) {
        $this->scalapayApi = $scalapayApi;
        $this->getAuthorization = $getAuthorization;
        $this->getOrderRefund = $getOrderRefund;
        $this->messageManager = $messageManager;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function placeRequest(TransferInterface $transferObject): array
    {
        $order = $transferObject->getBody()['payment']->getOrder();
        $orderToken = $order->getScalapayOrderToken();
        if ($orderToken === null) {
            throw new \Exception(__('Missing Scalapay Order Token'));
        }
        try {
            $this->scalapayApi::refund(
                $this->getAuthorization->execute(),
                $orderToken,
                $this->getOrderRefund->execute(
                    $transferObject->getBody()['payment']->getCreditmemo()
                )
            );
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return [];
    }
}
