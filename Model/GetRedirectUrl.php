<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Factory\Api as ScalapayApi;
use Scalapay\Scalapay\Model\Merchant\OrderToken;

class GetRedirectUrl
{
    /**
     * @var GetAuthorization
     */
    private $getAuthorization;
    /**
     * @var ScalapayApi
     */
    private $scalapayApi;
    /**
     * @var GetOrderDetails
     */
    private $getOrderDetails;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * GetRedirectUrl constructor.
     * @param GetAuthorization $getAuthorization
     * @param UrlInterface $url
     * @param ScalapayApi $scalapayApi
     * @param GetOrderDetails $getOrderDetails
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        GetAuthorization $getAuthorization,
        UrlInterface $url,
        ScalapayApi $scalapayApi,
        GetOrderDetails $getOrderDetails,
        ManagerInterface $messageManager
    ) {
        $this->getAuthorization = $getAuthorization;
        $this->scalapayApi = $scalapayApi;
        $this->getOrderDetails = $getOrderDetails;
        $this->messageManager = $messageManager;
        $this->url = $url;
    }

    /**
     * @param CartInterface $quote
     * @return string
     * @throws NoSuchEntityException
     */
    public function execute(CartInterface $quote): string
    {
        $authorization = $this->getAuthorization->execute();

        $orderDetails = $this->getOrderDetails->execute($quote);

        /** @var OrderToken $orderToken */
        try {
            $orderToken = $this->scalapayApi->createOrder(
                $authorization,
                $orderDetails
            );
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
            return $this->url->getUrl('checkout/cart');
        }

        return $orderToken->getCheckoutUrl();
    }
}
