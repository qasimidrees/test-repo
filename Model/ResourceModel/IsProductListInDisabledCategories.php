<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Scalapay\Scalapay\Gateway\Settings;

/**
 * Check if a set of products is in a disabled category
 */
class IsProductListInDisabledCategories
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param ResourceConnection $resourceConnection
     * @param Settings $settings
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        Settings $settings
    ) {
        $this->settings = $settings;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param int[] $productIds
     * @return bool
     */
    public function execute(array $productIds): bool
    {
        if (empty($productIds)) {
            return false;
        }

        $connection = $this->resourceConnection->getConnection();
        $catalogCategoryProductTableName = $connection->getTableName('catalog_category_product');
        $catalogCategoryEntityTableName = $connection->getTableName('catalog_category_entity');

        $qry = $connection
            ->select()
            ->distinct()
            ->from($catalogCategoryProductTableName, 'category_id')
            ->where('product_id IN (?)', $productIds);
        $leafCategoriesIds = $connection->fetchCol($qry);

        $qry = $connection
            ->select()
            ->from($catalogCategoryEntityTableName, 'path')
            ->where('entity_id IN (?)', $leafCategoriesIds);

        $categoriesPaths = $connection->fetchCol($qry);
        $productsCategories = array_unique(array_reduce($categoriesPaths, static function ($acc, $categoryPath) {
            return array_merge($acc, explode('/', $categoryPath));
        }, []));

        $disabledCategories = $this->settings->getDisabledCategories();

        return !empty(array_intersect($disabledCategories, $productsCategories));
    }
}
