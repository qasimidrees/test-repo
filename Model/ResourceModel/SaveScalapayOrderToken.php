<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;

class SaveScalapayOrderToken
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * SaveScalapayOrderToken constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param int $orderId
     * @param string $orderToken
     * @return void
     */
    public function execute(int $orderId, string $orderToken): void
    {
        $connection = $this->resourceConnection->getConnection();

        $connection->update(
            $this->resourceConnection->getTableName('sales_order'),
            ['scalapay_order_token' => $orderToken],
            'entity_id = ' . $orderId
        );
    }
}
