<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;

class SaveScalapayQuoteToken
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * SaveScalapayQuoteToken constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param int $orderId
     * @param string $orderToken
     * @return void
     */
    public function execute(int $quoteId, string $orderToken): void
    {
        $connection = $this->resourceConnection->getConnection();

        $connection->update(
            $this->resourceConnection->getTableName('quote'),
            ['scalapay_order_token' => $orderToken],
            'entity_id = ' . $quoteId
        );
    }
}
