<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Sales\Model\Order\Creditmemo;
use Scalapay\Scalapay\Model\Merchant\OrderRefund;

class GetOrderRefund
{
    /**
     * @param Creditmemo $creditmemo
     * @return OrderRefund
     */
    public function execute(Creditmemo $creditmemo): OrderRefund
    {
        $orderRefund = new OrderRefund();
        $orderRefund->setRefundAmount($creditmemo->getGrandTotal());
        $orderRefund->setMerchantReference($creditmemo->getOrderId());

        return $orderRefund;
    }
}
