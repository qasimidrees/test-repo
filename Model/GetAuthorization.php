<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Scalapay\Scalapay\Gateway\Settings;
use Scalapay\Scalapay\Model\Merchant\Authorization;

class GetAuthorization
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * GetAuthorization constructor.
     * @param Settings $settings
     */
    public function __construct(
        Settings $settings
    ) {
        $this->settings = $settings;
    }

    /**
     * @return Authorization
     */
    public function execute(): Authorization
    {
        if ($this->settings->getLiveMode()) {
            $authorization = new Authorization(
                Authorization::PRODUCTION_URI,
                $this->settings->getProductionApiKey()
            );
        } else {
            $authorization = new Authorization(
                Authorization::SANDBOX_URI,
                $this->settings->getTestApiKey()
            );
        }

        return $authorization;
    }
}
