<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Quote\Model\Quote;

class DoubleCheck
{
    /**
     * @var GetPaymentStatus
     */
    private $getPaymentStatus;

    /**
     * DoubleCheck constructor.
     * @param GetPaymentStatus $getPaymentStatus
     */
    public function __construct(
        GetPaymentStatus $getPaymentStatus
    ) {
        $this->getPaymentStatus = $getPaymentStatus;
    }

    /**
     * @param Quote $quote
     * @return bool
     * @noinspection PhpUndefinedMethodInspection
     */
    public function execute(Quote $quote): bool
    {
        $paymentStatus = $this->getPaymentStatus->execute($quote->getScalapayOrderToken());
        if ($quote->getScalapayOrderToken() !== $paymentStatus['token']
            || $quote->getGrandTotal() != $paymentStatus['totalAmount']['amount']
            || $quote->getReservedOrderId() !== $paymentStatus['orderDetails']['merchantReference']
            || $paymentStatus['status'] !== 'authorized'
            || $paymentStatus['captureStatus'] !== 'pending') {
            return false;
        }

        return true;
    }
}
