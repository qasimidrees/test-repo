<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Scalapay\Scalapay\Model\ResourceModel\SaveScalapayOrderToken;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class PlaceOrder
{
    /**
     * @var CreateOrderAfterCapture
     */
    private $createOrderAfterCapture;
    /**
     * @var SaveScalapayOrderToken
     */
    private $saveScalapayOrderToken;
    /**
     * @var QuoteFactory
     */
    private $quoteFactory;
    /**
     * @var DoubleCheck
     */
    private $doubleCheck;

    /**
     * PlaceOrder constructor.
     * @param CreateOrderAfterCapture $createOrderAfterCapture
     * @param SaveScalapayOrderToken $saveScalapayOrderToken
     * @param QuoteFactory $quoteFactory
     * @param DoubleCheck $doubleCheck
     */
    public function __construct(
        CreateOrderAfterCapture $createOrderAfterCapture,
        SaveScalapayOrderToken $saveScalapayOrderToken,
        QuoteFactory $quoteFactory,
        DoubleCheck $doubleCheck
    ) {
        $this->createOrderAfterCapture = $createOrderAfterCapture;
        $this->saveScalapayOrderToken = $saveScalapayOrderToken;
        $this->quoteFactory = $quoteFactory;
        $this->doubleCheck = $doubleCheck;
    }

    /**
     * @param CheckoutSession $checkoutSession
     * @param int $quoteId
     * @param string $orderToken
     * @return int
     * @throws LocalizedException
     * @noinspection PhpUndefinedMethodInspection
     */
    public function execute(
        CheckoutSession $checkoutSession,
        int $quoteId,
        string $orderToken
    ): int {
        /** @noinspection PhpDeprecationInspection */
        $quote = $this->quoteFactory->create()->load($quoteId);
        // Double check transaction validation
        if (!$this->doubleCheck->execute($quote)) {
            throw new AuthorizationException(__('Error on transaction validation (double check).'));
        }
        $checkoutSession->setPlaceOrder(true);
        /** @var OrderInterface|object|null $order */
        $orderId = (int) $this->createOrderAfterCapture->execute(
            $quote
        );
        $this->saveScalapayOrderToken->execute($orderId, $orderToken);
        $checkoutSession->unsPlaceOrder();

        return $orderId;
    }
}
