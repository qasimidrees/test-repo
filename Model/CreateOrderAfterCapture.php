<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\QuoteManagement;

class CreateOrderAfterCapture
{
    /**
     * @var QuoteManagement
     */
    private $quoteManagement;

    /**
     * CreateOrderAfterCapture constructor.
     * @param QuoteManagement $quoteManagement
     */
    public function __construct(
        QuoteManagement $quoteManagement
    ) {
        $this->quoteManagement = $quoteManagement;
    }

    /**
     * @param CartInterface $quote
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function execute(CartInterface $quote): int
    {
        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        return $this->quoteManagement->placeOrder($quote->getId(), $quote->getPayment());
    }
}
