<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Scalapay\Scalapay\Gateway\Settings;

/**
 * Check if language is allowed
 */
class IsLanguageAllowed
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param string $languageCode
     * @return bool
     */
    public function execute(string $languageCode): bool
    {
        if (!$this->settings->getAllowSpecificLanguages() || !$languageCode) {
            return true;
        }

        $allowedCountries = $this->settings->getSpecificLanguages();
        return in_array($languageCode, $allowedCountries);
    }
}
