<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Model\IsCountryAllowed;

class SpecificCountries implements FilterInterface
{
    /**
     * @var IsCountryAllowed
     */
    private $isCountryAllowed;

    /**
     * @param IsCountryAllowed $isCountryAllowed
     */
    public function __construct(
        IsCountryAllowed $isCountryAllowed
    ) {
        $this->isCountryAllowed = $isCountryAllowed;
    }

    /**
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool
    {
        $quoteBillingCountry = $quote->getBillingAddress()->getCountryId();
        return $this->isCountryAllowed->execute($quoteBillingCountry);
    }
}
