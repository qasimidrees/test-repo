<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Model\IsCurrencyAllowed;

class SpecificCurrencies implements FilterInterface
{
    /**
     * @var IsCurrencyAllowed
     */
    private $isCurrencyAllowed;

    /**
     * @param IsCurrencyAllowed $isCurrencyAllowed
     */
    public function __construct(
        IsCurrencyAllowed $isCurrencyAllowed
    ) {
        $this->isCurrencyAllowed = $isCurrencyAllowed;
    }

    /**
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool
    {
        $currencyCode = $quote->getStore()->getCurrentCurrencyCode();
        return $this->isCurrencyAllowed->execute($currencyCode);
    }
}
