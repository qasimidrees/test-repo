<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Magento\Quote\Api\Data\CartInterface;

class CartIsVirtual implements FilterInterface
{
    /**
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool
    {
        if ($quote->getIsVirtual()) {
            return false;
        }
        return true;
    }
}
