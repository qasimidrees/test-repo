<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Locale\Resolver;

/**
 * Scalapay config provider
 * @package Scalapay\Scalapay\Model
 */
class ScalapayConfigProvider implements ConfigProviderInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var Resolver
     */
    private $localeResolver;

    /**
     * @param Resolver $localeResolver
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Resolver $localeResolver,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->localeResolver = $localeResolver;
    }

    /**
     * @return string
     */
    private function getLogo(): string
    {
        $lang = $this->localeResolver->getLocale();

        if (substr($lang, 0, 2) === 'it') {
            return '<span class="scalapay_instalments_checkout_logo_italy"></span>';
        }

        return '<span class="scalapay_instalments_checkout_logo"></span>';
    }

    /**
     * @inheirtDoc
     */
    public function getConfig(): array
    {
        return [
            'payment' => [
                'scalapay' => [
                    'logo' => $this->getLogo(),
                    'title' => __($this->scopeConfig->getValue('payment/scalapay/checkout_title')),
                    'number_of_payments' =>
                        (int) $this->scopeConfig->getValue('payment/scalapay/number_of_payments'),
                ]
            ]
        ];
    }
}
