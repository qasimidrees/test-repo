<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Store\Model\StoreManagerInterface;
use Scalapay\Scalapay\Model\Merchant\Consumer;
use Scalapay\Scalapay\Model\Merchant\Contact;
use Scalapay\Scalapay\Model\Merchant\Discount;
use Scalapay\Scalapay\Model\Merchant\Item;
use Scalapay\Scalapay\Model\Merchant\MerchantOptions;
use Scalapay\Scalapay\Model\Merchant\Money;
use Scalapay\Scalapay\Model\Merchant\OrderDetails;

class GetOrderDetails
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * GetOrderDetails constructor.
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * @param CartInterface $quote
     * @return OrderDetails
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(CartInterface $quote): OrderDetails
    {
        // Total Amount
        $totalAmount = new Money();
        $totalAmount->setAmount($quote->getGrandTotal());

        // Customer
        $consumer = new Consumer();
        $consumer
            ->setEmail($quote->getBillingAddress()->getEmail())
            ->setGivenNames($quote->getBillingAddress()->getFirstname())
            ->setSurname($quote->getBillingAddress()->getLastname())
            ->setPhoneNumber($quote->getBillingAddress()->getTelephone());

        // Billing Address
        $billingAddress = new Contact();
        /** @noinspection PhpUndefinedMethodInspection */
        $billingAddress
            ->setName($quote->getBillingAddress()->getName())
            ->setLine1($quote->getBillingAddress()->getStreetLine(1))
            ->setLine2($quote->getBillingAddress()->getStreetLine(2))
            ->setPostcode($quote->getBillingAddress()->getPostcode())
            ->setSuburb($quote->getBillingAddress()->getCity())
            ->setCountryCode($quote->getBillingAddress()->getCountryId())
            ->setPhoneNumber($quote->getBillingAddress()->getTelephone());

        // Shippng Address
        $shippingAddress = new Contact();
        $shippingAddress
            ->setName($quote->getShippingAddress()->getName())
            ->setLine1($quote->getShippingAddress()->getStreetLine(1))
            ->setLine2($quote->getShippingAddress()->getStreetLine(2))
            ->setPostcode($quote->getShippingAddress()->getPostcode())
            ->setSuburb($quote->getShippingAddress()->getCity())
            ->setCountryCode($quote->getShippingAddress()->getCountryId())
            ->setPhoneNumber($quote->getShippingAddress()->getTelephone());

        // Items
        $items = [];
        $taxTotal = 0.00;
        /** @var \Magento\Quote\Model\Quote\Item $quoteItem */
        foreach ($quote->getAllVisibleItems() as $quoteItem) {
            $item = new Item();
            $item->setName($quoteItem->getName());
            $item->setSku($quoteItem->getSku());
            $item->setQuantity($quoteItem->getQty());
            $taxTotal += $quoteItem->getTaxAmount();
            $itemPrice = new Money();
            $itemPrice->setAmount((float) $quoteItem->getPrice());
            $item->setPrice($itemPrice);
            $items[] = $item;
        }

        // Discount
        $discountAmount = new Money();
        $discountAmount->setAmount($quote->getSubtotal() - $quote->getSubtotalWithDiscount());
        $discount = new Discount();
        $discount->setDisplayName('Total discount');
        /** @noinspection PhpParamsInspection */
        $discount->setAmount($discountAmount);
        $discountList[] = $discount;

        // Tax
        $taxAmount = new Money();
        $taxAmount->setAmount($taxTotal);

        // Shipping
        $shippingAmount = new Money();
        $shippingAmount->setAmount($quote->getShippingAddress()->getShippingAmount());

        // Merchant options
        $merchantOptions = new MerchantOptions();
        $merchantOptions->setRedirectConfirmUrl($this->storeManager->getStore()->getUrl('scalapay/confirm'));
        $merchantOptions->setRedirectCancelUrl($this->storeManager->getStore()->getUrl('scalapay/cancel'));

        $orderDetails = new OrderDetails();
        /** @noinspection PhpParamsInspection */
        return $orderDetails
            ->setTotalAmount($totalAmount)
            ->setConsumer($consumer)
            ->setBilling($billingAddress)
            ->setShipping($shippingAddress)
            ->setItems($items)
            ->setDiscounts($discountList)
            ->setTaxAmount($taxAmount)
            ->setShippingAmount($shippingAmount)
            ->setMerchant($merchantOptions)
            ->setMerchantReference($quote->getReservedOrderId());
    }
}
