<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Payment\Model\InfoInterface;

class GetPaymentInfo
{
    /**
     * @param InfoInterface $payment
     * @return array
     */
    public function execute(InfoInterface $payment): array
    {
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        return [
            'Transaction ID' => $payment->getOrder()->getScalapayOrderToken()
        ];
    }
}
