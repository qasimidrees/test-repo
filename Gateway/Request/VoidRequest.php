<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;

class VoidRequest implements BuilderInterface
{
    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        return [
            'payment' => $buildSubject['payment']->getPayment()
        ];
    }
}
