<?php
/*
 * Copyright © MageSpecialist - Skeeller srl. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Scalapay product page data view model
 */
class ProductPageData implements ArgumentInterface, DataViewModelInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param RequestInterface $request
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        RequestInterface $request,
        ProductRepositoryInterface $productRepository
    ) {
        $this->request = $request;
        $this->productRepository = $productRepository;
    }

    /**
     * @return int
     */
    private function getProductId(): int
    {
        return (int) $this->request->getParam('id');
    }

    /**
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    private function getProduct(): ProductInterface
    {
        return $this->productRepository->getById($this->getProductId());
    }

    /**
     * @return float
     * @throws NoSuchEntityException
     */
    public function getPrice(): float
    {
        $product = $this->getProduct();
        return (float) $product->getFinalPrice();
    }

    /**
     * @return array
     */
    public function getProductsIds(): array
    {
        return [$this->getProductId()];
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return '';
    }
}
